﻿namespace ShopOfBikes.DAL.Shared
{
    public class Enums
    {
        public enum OrderStatus
        {
            Started,
            Cancelled,
            Completed
        }
    }
}
