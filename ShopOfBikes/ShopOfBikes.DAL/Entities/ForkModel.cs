﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ShopOfBikes.DAL.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class ForkModel  //Производитель вилки
    {
        [Column("ForkModelId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
