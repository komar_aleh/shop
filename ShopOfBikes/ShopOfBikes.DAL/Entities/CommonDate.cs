﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ShopOfBikes.DAL.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class CommonDate  //Год выхода на рынок
    {
        [Column("CommonDateId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(4)]
        public int Value { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
