﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ShopOfBikes.DAL.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class TiresModel  //Модель покрышек
    {
        [Column("TiresModelId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
