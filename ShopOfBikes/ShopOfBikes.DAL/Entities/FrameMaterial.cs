﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ShopOfBikes.DAL.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class FrameMaterial  //Материал рамы
    {
        [Column("FrameMaterialId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
