﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class FrontSwitch //Передний переключатель
    {
        [Column("FrontSwitchId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
