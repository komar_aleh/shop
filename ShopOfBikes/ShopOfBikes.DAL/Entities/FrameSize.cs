﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ShopOfBikes.DAL.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class FrameSize //Размер рамы
    {
        [Column("FrameSizeId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
