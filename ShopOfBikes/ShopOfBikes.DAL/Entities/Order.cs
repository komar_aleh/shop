﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ShopOfBikes.DAL.Shared;

namespace ShopOfBikes.DAL.Entities
{
    public class Order
    {
        [Required]
        [Column("OrderId")]
        [Key]
        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        public Enums.OrderStatus Status{ get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; } = new List<OrderItem>();

        public Order()
        {
            OrderDate = DateTime.Now;
            Status = Enums.OrderStatus.Started;
        }
    }
}
