﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class AbsorberStroke  //Ход амортизатора
    {
        [Column("AbsorberStrokeId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(3)]
        public int Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }

    }
}
