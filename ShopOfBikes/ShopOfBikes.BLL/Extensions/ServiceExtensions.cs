﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using ShopOfBikes.BLL.Filters;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Services;
using System.Reflection;
using Microsoft.Extensions.Localization;
using ShopOfBikes.BLL.Interfaces.Repositories;
using ShopOfBikes.BLL.Interfaces.Repositories.Property;
using ShopOfBikes.BLL.Repositories;
using ShopOfBikes.BLL.Repositories.Property;

namespace ShopOfBikes.BLL.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddBusinessLogicLayer(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddHttpContextAccessor();
            services.AddSingleton<IUriHelper>(o =>
            {
                var accessor = o.GetRequiredService<IHttpContextAccessor>();
                var request = accessor.HttpContext.Request;
                var uri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());
                return new UriHelper(uri);
            });
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddScoped<TokenAuthorizationFilter>();

            #region Repositories
            services.AddScoped<IProductRepositoryAsync, ProductRepositoryAsync>();
            services.AddScoped<IOrderRepositoryAsync, OrderRepositoryAsync>();
            services.AddScoped<IOrderItemRepositoryAsync, OrderItemRepositoryAsync>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IManufacturerRepositoryAsync, ManufacturerRepositoryAsync>();
            services.AddScoped<IClassOfBikeRepositoryAsync, ClassOfBikeRepositoryAsync>();
            services.AddScoped<IFrameMaterialRepositoryAsync, FrameMaterialRepositoryAsync>();
            services.AddScoped<IAbsorberStrokeRepositoryAsync, AbsorberStrokeRepositoryAsync>();
            services.AddScoped<ICassetteRepositoryAsync, CassetteRepositoryAsync>();
            services.AddScoped<IChainRepositoryAsync, ChainRepositoryAsync>();
            services.AddScoped<ICommonDateRepositoryAsync, CommonDateRepositoryAsync>();
            services.AddScoped<IForkMaterialRepositoryAsync, ForkMaterialRepositoryAsync>();
            services.AddScoped<IForkModelRepositoryAsync, ForkModelRepositoryAsync>();
            services.AddScoped<IForkTypeRepositoryAsync, ForkTypeRepositoryAsync>();
            services.AddScoped<IFrameColorRepositoryAsync, FrameColorRepositoryAsync>();
            services.AddScoped<IFrameMaterialRepositoryAsync, FrameMaterialRepositoryAsync>();
            services.AddScoped<IFrameSizeRepositoryAsync, FrameSizeRepositoryAsync>();
            services.AddScoped<IFrontBrakeModelRepositoryAsync, FrontBrakeModelRepositoryAsync>();
            services.AddScoped<IFrontBrakeTypeRepositoryAsync, FrontBrakeTypeRepositoryAsync>();
            services.AddScoped<IFrontStarRepositoryAsync, FrontStarRepositoryAsync>();
            services.AddScoped<IFrontSwitchRepositoryAsync, FrontSwitchRepositoryAsync>();
            services.AddScoped<IGripsModelRepositoryAsync, GripsModelRepositoryAsync>();
            services.AddScoped<IHandlebarModelRepositoryAsync, HandlebarModelRepositoryAsync>();
            services.AddScoped<IHandlebarTypeRepositoryAsync, HandlebarTypeRepositoryAsync>();
            services.AddScoped<IPedalsMaterialRepositoryAsync, PedalsMaterialRepositoryAsync>();
            services.AddScoped<IPedalsModelRepositoryAsync, PedalsModelRepositoryAsync>();
            services.AddScoped<IPedalsTypeRepositoryAsync, PedalsTypeRepositoryAsync>();
            services.AddScoped<IRearBrakeModelRepositoryAsync, RearBrakeModelRepositoryAsync>();
            services.AddScoped<IRearBrakeTypeRepositoryAsync, RearBrakeTypeRepositoryAsync>();
            services.AddScoped<IRearStarRepositoryAsync, RearStarRepositoryAsync>();
            services.AddScoped<IRearSwitchRepositoryAsync, RearSwitchRepositoryAsync>();
            services.AddScoped<IRimMaterialRepositoryAsync, RimMaterialRepositoryAsync>();
            services.AddScoped<ISaddleModelRepositoryAsync, SaddleModelRepositoryAsync>();
            services.AddScoped<ISaddleTypeRepositoryAsync, SaddleTypeRepositoryAsync>();
            services.AddScoped<IShifterModelRepositoryAsync, ShifterModelRepositoryAsync>();
            services.AddScoped<IShifterTypeRepositoryAsync, ShifterTypeRepositoryAsync>();
            services.AddScoped<ISpeedNumberRepositoryAsync, SpeedNumberRepositoryAsync>();
            services.AddScoped<ISystemModelRepositoryAsync, SystemModelRepositoryAsync>();
            services.AddScoped<ITiresModelRepositoryAsync, TiresModelRepositoryAsync>();
            services.AddScoped<IWheelDiameterRepositoryAsync, WheelDiameterRepositoryAsync>();
            #endregion Repositories
        }
    }
}