﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Product;

namespace ShopOfBikes.BLL.Validation.Product
{
    public class ProductForCreationDtoValidator : AbstractValidator<ProductForCreationDto>
    {
        public ProductForCreationDtoValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .Length(2, 50).WithMessage("{PropertyName} must be between 2 and 50 characters");

            RuleFor(x => x.Price)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");

            RuleFor(x => x.ManufacturerId)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");

            RuleFor(x => x.Quantity)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .GreaterThanOrEqualTo(0u).WithMessage("{PropertyName} must be greater than or equal to 0");
        }
    }
}
