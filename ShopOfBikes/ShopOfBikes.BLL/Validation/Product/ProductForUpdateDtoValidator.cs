﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Product;

namespace ShopOfBikes.BLL.Validation.Product
{
    public class ProductForUpdateDtoValidator : AbstractValidator<ProductForUpdateDto>
    {
        public ProductForUpdateDtoValidator()
        {
            RuleFor(x => x.Name)

                .Length(2, 50).WithMessage("{PropertyName} must be between 2 and 50 characters");

            RuleFor(x => x.Price)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");

            RuleFor(x => x.ManufacturerId)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");

            RuleFor(x => x.Quantity)
                .GreaterThanOrEqualTo(0u).WithMessage("{PropertyName} must be greater than or equal to 0");
        }
    }
}
