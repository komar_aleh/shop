﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Order;

namespace ShopOfBikes.BLL.Validation.Order
{
    public class OrderForUpdateDtoValidator : AbstractValidator<OrderForUpdateDto>
    {
        public OrderForUpdateDtoValidator()
        {
            RuleFor(x => x.Status)
                .IsInEnum();
        }
    }
}
