﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Order;

namespace ShopOfBikes.BLL.Validation.Order
{
    public class OrderForCreationDtoValidator : AbstractValidator<OrderForCreationDto>
    {
        public OrderForCreationDtoValidator()
        {
            RuleFor(x=>x.UserId)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");
        }
    }
}
