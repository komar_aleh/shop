﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.OrderItem;

namespace ShopOfBikes.BLL.Validation.OrderItem
{
    public class OrderItemForCreationDtoValidator : AbstractValidator<OrderItemForCreationDto>
    {
        public OrderItemForCreationDtoValidator()
        {
            RuleFor(x => x.OrderId)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .LessThan(2);  //fdsfdsfsdfsdf
            RuleFor(x => x.ProductId)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");
                
            RuleFor(x => x.Quantity)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .LessThan(100);
        }
    }
}
