﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.OrderItem;

namespace ShopOfBikes.BLL.Validation.OrderItem
{
    public class OrderItemForUpdateDtoValidator : AbstractValidator<OrderItemForUpdateDto>
    {
        public OrderItemForUpdateDtoValidator()
        {
            RuleFor(x => x.ProductId)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");
            RuleFor(x => x.Quantity)
                .NotEmpty().WithMessage("{PropertyName} can't be empty");
        }
    }
}
