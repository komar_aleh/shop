﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.Validation.Property
{
    public class PropertyIntValueForCreationUpdateDtoValidator : AbstractValidator<PropertyIntValueForCreationUpdateDto>
    {
        public PropertyIntValueForCreationUpdateDtoValidator()
        {
            RuleFor(x => x.Value)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .NotNull()
                .LessThan(200)
                .GreaterThan(100);
        }
    }
}
