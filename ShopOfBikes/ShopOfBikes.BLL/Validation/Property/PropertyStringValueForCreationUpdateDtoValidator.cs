﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.Validation.Property
{
    public class PropertyStringValueForCreationUpdateDtoValidator : AbstractValidator<PropertyStringValueForCreationUpdateDto>
    {
        public PropertyStringValueForCreationUpdateDtoValidator()
        {
            RuleFor(x => x.Value)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .NotNull();
        }
    }
}
