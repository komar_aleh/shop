﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.Validation.Property
{
    public class PropertyStringValueDtoValidator : AbstractValidator<PropertyStringValueDto>
    {
        public PropertyStringValueDtoValidator()
        {
            RuleFor(x => x.Value)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .NotNull();
        }
    }
}
