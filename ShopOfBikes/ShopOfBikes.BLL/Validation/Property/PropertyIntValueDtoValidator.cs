﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.Validation.Property
{
    public class PropertyIntValueDtoValidator : AbstractValidator<PropertyIntValueDto>
    {
        public PropertyIntValueDtoValidator()
        {
            RuleFor(x => x.Value)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .NotNull();
        }
    }
}
