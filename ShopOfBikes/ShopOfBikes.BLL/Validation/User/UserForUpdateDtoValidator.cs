﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.User;

namespace ShopOfBikes.BLL.Validation.User
{
    public class UserForUpdateDtoValidator : AbstractValidator<UserForUpdateDto>
    {
        public UserForUpdateDtoValidator()
        {
            RuleFor(x => x.Email)
                .EmailAddress();
        }
    }
}
