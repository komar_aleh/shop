﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.User;

namespace ShopOfBikes.BLL.Validation.User
{
    public class UserLoginDtoValidator : AbstractValidator<UserForLoginDto>
    {
        public UserLoginDtoValidator()
        {
            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .Length(2, 20).WithMessage("{PropertyName} must be between 2 and 20 characters");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("{PropertyName} can't be empty")
                .MinimumLength(6).WithMessage("Password must be at least 6 characters")
                .MaximumLength(100).WithMessage("Password must be at least 100 characters")
                /*.Matches("[A-Z]").WithMessage("Password must contain 1 uppercase letter")
                .Matches("[a-z]").WithMessage("Password must contain 1 lowercase letter")
                .Matches("[0-9]").WithMessage("Password must contain a number")
                .Matches("[^a-zA-Z0-9]").WithMessage("Password must contain non alphanumeric")
                .Matches("^(?=\\S+$).{8,}$").WithMessage("Password may not contain whitespaces!")*/;
        }
    }
}
