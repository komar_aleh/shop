﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace ShopOfBikes.BLL.Filters
{
    public class TokenAuthorizationFilter : /*Attribute, */IAuthorizationFilter
    {
        private readonly IConfiguration _configuration;
        
        public TokenAuthorizationFilter(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var loggerAuth = context.HttpContext.RequestServices.GetService<ILogger<TokenAuthorizationFilter>>();
            var issuer = _configuration["JWT:ServerUrl"];
            var mySecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:SecretKey"]));
            var token = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

                var principal = new JwtSecurityTokenHandler().ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = issuer,
                    ValidAudience = issuer,
                    IssuerSigningKey = mySecurityKey
                }, out var validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;

            if (jwtToken.Payload.Count == 8 && jwtToken.Issuer == issuer  ) 
            {
                loggerAuth.LogInformation($"Info --- Authorization success"); 
            }
            else
            {
                loggerAuth.LogError("Error --- Access denied. You must have the role - 'Admin'");
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
