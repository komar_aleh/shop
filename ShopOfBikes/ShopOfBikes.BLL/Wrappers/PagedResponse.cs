﻿using System;
using ShopOfBikes.BLL.Parameters;

namespace ShopOfBikes.BLL.Wrappers
{
    public class PagedResponse<T> : Response<T>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Uri FirstPage { get; set; }
        public Uri LastPage { get; set; }
        public int TotalPages { get; set; }
        public int RecordsFiltered { get; set; }
        public int TotalRecords { get; set; }
        public Uri NextPage { get; set; }
        public Uri PreviousPage { get; set; }
        public PagedResponse(T data, int pageNumber, int pageSize, RecordsCount recordsCount)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            RecordsFiltered = recordsCount.RecordsFiltered;
            TotalRecords = recordsCount.RecordsTotal;
            TotalPages = (int)Math.Ceiling(recordsCount.RecordsFiltered / (double)pageSize);
            Data = data;
            Message = null;
            Succeeded = true;
            Errors = null;
        }
    }
}
