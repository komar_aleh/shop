﻿using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;

namespace ShopOfBikes.BLL.Helpers
{
    public class PaginationHelper
    {
        public static PagedResponse<IEnumerable<T>> CreatePagedResponse<T>(IEnumerable<T> pagedData, QueryParameters queryParameters, IUriHelper uriService, string route, RecordsCount recordsCount)
        {
            var response = new PagedResponse<IEnumerable<T>>(pagedData, queryParameters.PageNumber, queryParameters.PageSize, recordsCount);
            response.NextPage =
                queryParameters.PageNumber >= 1 && queryParameters.PageNumber < response.TotalPages
                    ? uriService.GetPageUri(new QueryParameters() { PageNumber = queryParameters.PageNumber + 1, PageSize = queryParameters.PageSize }, route)
                    : null;
            response.PreviousPage =
                queryParameters.PageNumber - 1 >= 1 && queryParameters.PageNumber <= response.TotalPages
                    ? uriService.GetPageUri(new QueryParameters() { PageNumber = queryParameters.PageNumber - 1, PageSize = queryParameters.PageSize }, route)
                    : null;
            response.FirstPage = uriService.GetPageUri(new QueryParameters() { PageNumber = 1, PageSize = queryParameters.PageSize }, route);
            response.LastPage = uriService.GetPageUri(new QueryParameters() { PageNumber = response.TotalPages, PageSize = queryParameters.PageSize }, route);
            response.Message = $"Returned { recordsCount.RecordsFiltered } records";
            return response;
        }
    }
}