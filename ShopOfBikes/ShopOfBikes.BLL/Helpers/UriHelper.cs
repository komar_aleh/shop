﻿using System;
using Microsoft.AspNetCore.WebUtilities;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Parameters;

namespace ShopOfBikes.BLL.Helpers
{
    public class UriHelper : IUriHelper
    {
        private readonly string _baseUri;
        public UriHelper(string baseUri)
        {
            _baseUri = baseUri;
        }
        public Uri GetPageUri(QueryParameters queryParameters, string route)
        {
            var endpointUri = new Uri(string.Concat(_baseUri, route));
            var modifiedUri = QueryHelpers.AddQueryString(endpointUri.ToString(), "pageNumber", queryParameters.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", queryParameters.PageSize.ToString());
            return new Uri(modifiedUri);
        }
    }
}
