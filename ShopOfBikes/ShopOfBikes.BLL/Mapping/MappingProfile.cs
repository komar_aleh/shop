﻿using AutoMapper;
using ShopOfBikes.BLL.DTO.Order;
using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.DTO.Product;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.DTO.User;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<Product, ProductDetailsDto>();
            CreateMap<ProductForCreationDto, Product>();
            CreateMap<ProductForUpdateDto, Product>();

            CreateMap<Order, OrderDto>();
            CreateMap<Order, OrderAfterCreationDto>();
            CreateMap<OrderForCreationDto, Order>();
            CreateMap<OrderForUpdateDto, Order>();

            CreateMap<OrderItem, OrderItemDto>();
            CreateMap<OrderItem, OrderItemWithoutProductDto>();
            CreateMap<OrderItemForCreationDto, OrderItem>();
            CreateMap<OrderItemForUpdateDto, OrderItem>();

            CreateMap<User, UserDto>();
            CreateMap<User, UserWithOrdersDto>();
            CreateMap<UserForUpdateDto, User>();

            CreateMap<AbsorberStroke, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, AbsorberStroke>();

            CreateMap<Cassette, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, Cassette>();

            CreateMap<Chain, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, Chain>();

            CreateMap<ClassOfBike, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ClassOfBike>();

            CreateMap<CommonDate, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, CommonDate>();

            CreateMap<ForkMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ForkMaterial>();

            CreateMap<ForkModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ForkModel>();

            CreateMap<ForkType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ForkType>();

            CreateMap<FrameColor, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrameColor>();

            CreateMap<FrameMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrameMaterial>();

            CreateMap<FrameSize, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrameSize>();

            CreateMap<FrontBrakeModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrontBrakeModel>();

            CreateMap<FrontBrakeType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrontBrakeType>();

            CreateMap<FrontStar, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, FrontStar>();

            CreateMap<FrontSwitch, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrontSwitch>();

            CreateMap<GripsModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, GripsModel>();

            CreateMap<HandlebarModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, HandlebarModel>();

            CreateMap<HandlebarType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, HandlebarType>();

            CreateMap<Manufacturer, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, Manufacturer>();

            CreateMap<PedalsMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, PedalsMaterial>();

            CreateMap<PedalsModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, PedalsModel>();

            CreateMap<PedalsType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, PedalsType>();

            CreateMap<RearBrakeModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RearBrakeModel>();

            CreateMap<RearBrakeType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RearBrakeType>();

            CreateMap<RearStar, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, RearStar>();

            CreateMap<RearSwitch, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RearSwitch>();

            CreateMap<RimMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RimMaterial>();

            CreateMap<SaddleModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, SaddleModel>();

            CreateMap<SaddleType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, SaddleType>();

            CreateMap<ShifterModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ShifterModel>();

            CreateMap<ShifterType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ShifterType>();

            CreateMap<SpeedNumber, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, SpeedNumber>();

            CreateMap<SystemModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, SystemModel>();

            CreateMap<TiresModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, TiresModel>();

            CreateMap<WheelDiameter, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, WheelDiameter>();
        }
    }
}
