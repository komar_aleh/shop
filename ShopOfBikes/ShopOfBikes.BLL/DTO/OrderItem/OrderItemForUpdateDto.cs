﻿namespace ShopOfBikes.BLL.DTO.OrderItem
{
    public class OrderItemForUpdateDto
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
