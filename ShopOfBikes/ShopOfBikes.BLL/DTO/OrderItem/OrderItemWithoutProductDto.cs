﻿namespace ShopOfBikes.BLL.DTO.OrderItem
{
    public class OrderItemWithoutProductDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
    }
}
