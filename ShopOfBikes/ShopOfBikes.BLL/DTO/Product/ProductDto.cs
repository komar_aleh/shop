﻿using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.DTO.Product
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public int? ManufacturerId { get; set; }
        public PropertyStringValueDto Manufacturer { get; set; }
        public string ImgName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        
    }
}
