﻿namespace ShopOfBikes.BLL.DTO.Product
{
    public class ProductForUpdateDto
    {
        public string Name { get; set; }
        public uint Quantity { get; set; }
        public decimal Price { get; set; }
        public int ManufacturerId { get; set; }
        public int? ClassOfBikeId { get; set; }
        public int? CommonDateId { get; set; }
        public string ImgName { get; set; }
        public float? Weight { get; set; }
        public bool? HardTail { get; set; }
        public bool? Female { get; set; }
        public bool? KidTeen { get; set; }
        public bool? DoubleRim { get; set; }
        public bool? TiresTubeless { get; set; }
        public bool? ForkLock { get; set; }
        public int? FrameMaterialId { get; set; }
        public int? FrameSizeId { get; set; }
        public int? ForkMaterialId { get; set; }
        public int? WheelDiameterId { get; set; }
        public int? FrameColorId { get; set; }
        public int? ForkModelId { get; set; }
        public int? ForkTypeId { get; set; }
        public int? AbsorberStrokeId { get; set; }
        public int? SpeedNumberId { get; set; }
        public int? SystemModelId { get; set; }
        public int? FrontStarId { get; set; }
        public int? RearStarId { get; set; }
        public int? CassetteId { get; set; }
        public int? FrontSwitchId { get; set; }
        public int? RearSwitchId { get; set; }
        public int? ChainId { get; set; }
        public int? ShifterModelId { get; set; }
        public int? ShifterTypeId { get; set; }
        public int? FrontBrakeModelId { get; set; }
        public int? FrontBrakeTypeId { get; set; }
        public int? RearBrakeModelId { get; set; }
        public int? RearBrakeTypeId { get; set; }
        public int? RimMaterialId { get; set; }
        public int? TiresModelId { get; set; }
        public int? HandlebarModelId { get; set; }
        public int? HandlebarTypeId { get; set; }
        public int? GripsModelId { get; set; }
        public int? SaddleTypeId { get; set; }
        public int? SaddleModelId { get; set; }
        public int? PedalsTypeId { get; set; }
        public int? PedalsModelId { get; set; }
        public int? PedalsMaterialId { get; set; }
    }
}
