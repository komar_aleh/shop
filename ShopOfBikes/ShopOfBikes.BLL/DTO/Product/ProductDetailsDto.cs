﻿using ShopOfBikes.BLL.DTO.Property;
using System;

namespace ShopOfBikes.BLL.DTO.Product
{
    public class ProductDetailsDto
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string ImgName { get; set; }
        //public int ManufacturerId { get; set; }
        public PropertyStringValueDto Manufacturer { get; set; }
        //public int ClassOfBikeId { get; set; }
        public PropertyStringValueDto ClassOfBike { get; set; }
        //public int CommonDateId { get; set; }
        public PropertyIntValueDto CommonDate { get; set; }
        public float Weight { get; set; }
        //public int FrameMaterialId { get; set; }
        public PropertyStringValueDto FrameMaterial { get; set; }
        //public int FrameSizeId { get; set; }
        public PropertyStringValueDto FrameSize { get; set; }
        //public int ForkMaterialId { get; set; }
        public PropertyStringValueDto ForkMaterial { get; set; }
        //public int WheelDiameterId { get; set; }
        public PropertyStringValueDto WheelDiameter { get; set; }
        //public int FrameColorId { get; set; }
        public PropertyStringValueDto FrameColor { get; set; }
        //public int ForkModelId { get; set; }
        public PropertyStringValueDto ForkModel { get; set; }
        //public int ForkTypeId { get; set; }
        public PropertyStringValueDto ForkType { get; set; }
        //public int AbsorberStrokeId { get; set; }
        public PropertyIntValueDto AbsorberStroke { get; set; }
        //public int SpeedNumberId { get; set; }
        public PropertyStringValueDto SpeedNumber { get; set; }
        //public int SystemModelId { get; set; }
        public PropertyStringValueDto SystemModel { get; set; }
        //public int FrontStarId { get; set; }
        public PropertyIntValueDto FrontStar { get; set; }
        //public int RearStarId { get; set; }
        public PropertyIntValueDto RearStar { get; set; }
        //public int CassetteId { get; set; }
        public PropertyStringValueDto Cassette { get; set; }
        public bool HardTail { get; set; }
        public bool Female { get; set; } 
        public bool KidTeen { get; set; }
        public bool DoubleRim { get; set; }
        public bool TiresTubeless { get; set; }
        public bool ForkLock { get; set; }
        //public int FrontSwitchId { get; set; }
        public PropertyStringValueDto FrontSwitch { get; set; }
        //public int RearSwitchId { get; set; }
        public PropertyStringValueDto RearSwitch { get; set; }
        //public int ChainId { get; set; }
        public PropertyStringValueDto Chain { get; set; }
        //public int ShifterModelId { get; set; }
        public PropertyStringValueDto ShifterModel { get; set; }
        //public int ShifterTypeId { get; set; }
        public PropertyStringValueDto ShifterType { get; set; }
        //public int FrontBrakeModelId { get; set; }
        public PropertyStringValueDto FrontBrakeModel { get; set; }
        //public int FrontBrakeTypeId { get; set; }
        public PropertyStringValueDto FrontBrakeType { get; set; }
        //public int RearBrakeModelId { get; set; }
        public PropertyStringValueDto RearBrakeModel { get; set; }
        //public int RearBrakeTypeId { get; set; }
        public PropertyStringValueDto RearBrakeType { get; set; }
        //public int RimMaterialId { get; set; }
        public PropertyStringValueDto RimMaterial { get; set; }
        //public int TiresModelId { get; set; }
        public PropertyStringValueDto TiresModel { get; set; }
        //public int HandlebarModelId { get; set; }
        public PropertyStringValueDto HandlebarModel { get; set; }
        //public int HandlebarTypeId { get; set; }
        public PropertyStringValueDto HandlebarType { get; set; }
        //public int GripsModelId { get; set; }
        public PropertyStringValueDto GripsModel { get; set; }
        //public int SaddleTypeId { get; set; }
        public PropertyStringValueDto SaddleType { get; set; }
        //public int SaddleModelId { get; set; }
        public PropertyStringValueDto SaddleModel { get; set; }
        //public int PedalsTypeId { get; set; }
        public PropertyStringValueDto PedalsType { get; set; }
        //public int PedalsModelId { get; set; }
        public PropertyStringValueDto PedalsModel { get; set; }
        //public int PedalsMaterialId { get; set; }
        public PropertyStringValueDto PedalsMaterial { get; set; }
    }
}
