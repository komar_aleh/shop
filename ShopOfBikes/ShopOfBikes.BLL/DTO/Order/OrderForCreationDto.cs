﻿namespace ShopOfBikes.BLL.DTO.Order
{
    public class OrderForCreationDto
    {
        public string UserId { get; set; }
    }
}
