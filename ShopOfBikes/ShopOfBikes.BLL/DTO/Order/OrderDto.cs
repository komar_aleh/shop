﻿using ShopOfBikes.BLL.DTO.OrderItem;
using System;
using System.Collections.Generic;

namespace ShopOfBikes.BLL.DTO.Order
{
    public class OrderDto
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; }
        public List<OrderItemDto> OrderItems { get; set; }
    }
}
