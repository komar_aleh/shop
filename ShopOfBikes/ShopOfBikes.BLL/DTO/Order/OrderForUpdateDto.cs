﻿using ShopOfBikes.DAL.Shared;

namespace ShopOfBikes.BLL.DTO.Order
{
    public class OrderForUpdateDto
    {
        public Enums.OrderStatus Status { get; set; }
    }
}
