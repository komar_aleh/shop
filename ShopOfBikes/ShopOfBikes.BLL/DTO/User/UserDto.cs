﻿using System.Collections.Generic;
using ShopOfBikes.BLL.DTO.Order;

namespace ShopOfBikes.BLL.DTO.User
{
    public class UserDto 
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
