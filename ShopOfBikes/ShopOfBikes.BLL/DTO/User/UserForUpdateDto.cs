﻿namespace ShopOfBikes.BLL.DTO.User
{
    public class UserForUpdateDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
    }
}
