﻿namespace ShopOfBikes.BLL.DTO.Property
{
    public class PropertyStringValueDto
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
