﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IForkTypeRepositoryAsync : IGenericRepositoryBaseAsync<ForkType>
    {
        public Task<(IEnumerable<ForkType> data, RecordsCount recordsCount)> GetPagedForkTypeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
