﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface ITiresModelRepositoryAsync : IGenericRepositoryBaseAsync<TiresModel>
    {
        public Task<(IEnumerable<TiresModel> data, RecordsCount recordsCount)> GetPagedTiresModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
