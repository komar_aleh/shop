﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface ICommonDateRepositoryAsync : IGenericRepositoryBaseAsync<CommonDate>
    {
        public Task<(IEnumerable<CommonDate> data, RecordsCount recordsCount)> GetPagedCommonDateResponseAsync(
            IntPropertyParameters requestParameters);
    }
}
