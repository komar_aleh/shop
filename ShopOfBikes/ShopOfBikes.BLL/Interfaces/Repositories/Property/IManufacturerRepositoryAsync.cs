﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IManufacturerRepositoryAsync : IGenericRepositoryBaseAsync<Manufacturer>
    {
        public Task<(IEnumerable<Manufacturer> data, RecordsCount recordsCount)> GetPagedManufacturerResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
