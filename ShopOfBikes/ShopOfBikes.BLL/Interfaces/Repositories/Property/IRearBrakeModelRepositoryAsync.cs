﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IRearBrakeModelRepositoryAsync : IGenericRepositoryBaseAsync<RearBrakeModel>
    {
        public Task<(IEnumerable<RearBrakeModel> data, RecordsCount recordsCount)> GetPagedRearBrakeModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
