﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IShifterModelRepositoryAsync : IGenericRepositoryBaseAsync<ShifterModel>
    {
        public Task<(IEnumerable<ShifterModel> data, RecordsCount recordsCount)> GetPagedShifterModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
