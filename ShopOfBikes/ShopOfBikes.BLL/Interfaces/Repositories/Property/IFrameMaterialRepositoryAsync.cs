﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IFrameMaterialRepositoryAsync : IGenericRepositoryBaseAsync<FrameMaterial>
    {
        public Task<(IEnumerable<FrameMaterial> data, RecordsCount recordsCount)> GetPagedFrameMaterialResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
