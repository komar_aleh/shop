﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IHandlebarModelRepositoryAsync : IGenericRepositoryBaseAsync<HandlebarModel>
    {
        public Task<(IEnumerable<HandlebarModel> data, RecordsCount recordsCount)> GetPagedHandlebarModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
