﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface ICassetteRepositoryAsync : IGenericRepositoryBaseAsync<Cassette>
    {
        public Task<(IEnumerable<Cassette> data, RecordsCount recordsCount)> GetPagedCassetteResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
