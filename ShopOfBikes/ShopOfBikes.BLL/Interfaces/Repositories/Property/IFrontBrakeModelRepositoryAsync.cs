﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IFrontBrakeModelRepositoryAsync : IGenericRepositoryBaseAsync<FrontBrakeModel>
    {
        public Task<(IEnumerable<FrontBrakeModel> data, RecordsCount recordsCount)> GetPagedFrontBrakeModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
