﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IPedalsMaterialRepositoryAsync : IGenericRepositoryBaseAsync<PedalsMaterial>
    {
        public Task<(IEnumerable<PedalsMaterial> data, RecordsCount recordsCount)> GetPagedPedalsMaterialResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
