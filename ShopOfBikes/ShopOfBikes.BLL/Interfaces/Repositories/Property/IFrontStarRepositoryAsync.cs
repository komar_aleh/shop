﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IFrontStarRepositoryAsync : IGenericRepositoryBaseAsync<FrontStar>
    {
        public Task<(IEnumerable<FrontStar> data, RecordsCount recordsCount)> GetPagedFrontStarResponseAsync(
            IntPropertyParameters requestParameters);
    }
}
