﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IPedalsModelRepositoryAsync : IGenericRepositoryBaseAsync<PedalsModel>
    {
        public Task<(IEnumerable<PedalsModel> data, RecordsCount recordsCount)> GetPagedPedalsModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
