﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface ISpeedNumberRepositoryAsync : IGenericRepositoryBaseAsync<SpeedNumber>
    {
        public Task<(IEnumerable<SpeedNumber> data, RecordsCount recordsCount)> GetPagedSpeedNumberResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
