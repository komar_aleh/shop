﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IClassOfBikeRepositoryAsync : IGenericRepositoryBaseAsync<ClassOfBike>
    {
        public Task<(IEnumerable<ClassOfBike> data, RecordsCount recordsCount)> GetPagedClassOfBikeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
