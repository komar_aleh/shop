﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IChainRepositoryAsync : IGenericRepositoryBaseAsync<Chain>
    {
        public Task<(IEnumerable<Chain> data, RecordsCount recordsCount)> GetPagedChainResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
