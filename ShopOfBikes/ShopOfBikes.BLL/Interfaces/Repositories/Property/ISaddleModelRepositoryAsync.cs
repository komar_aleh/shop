﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface ISaddleModelRepositoryAsync : IGenericRepositoryBaseAsync<SaddleModel>
    {
        public Task<(IEnumerable<SaddleModel> data, RecordsCount recordsCount)> GetPagedSaddleModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
