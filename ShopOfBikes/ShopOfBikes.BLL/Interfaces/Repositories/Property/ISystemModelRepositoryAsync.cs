﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface ISystemModelRepositoryAsync : IGenericRepositoryBaseAsync<SystemModel>
    {
        public Task<(IEnumerable<SystemModel> data, RecordsCount recordsCount)> GetPagedSystemModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
