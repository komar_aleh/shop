﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IFrontSwitchRepositoryAsync : IGenericRepositoryBaseAsync<FrontSwitch>
    {
        public Task<(IEnumerable<FrontSwitch> data, RecordsCount recordsCount)> GetPagedFrontSwitchResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
