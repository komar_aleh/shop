﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IGripsModelRepositoryAsync : IGenericRepositoryBaseAsync<GripsModel>
    {
        public Task<(IEnumerable<GripsModel> data, RecordsCount recordsCount)> GetPagedGripsModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
