﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IForkModelRepositoryAsync : IGenericRepositoryBaseAsync<ForkModel>
    {
        public Task<(IEnumerable<ForkModel> data, RecordsCount recordsCount)> GetPagedForkModelResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
