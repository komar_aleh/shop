﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IFrontBrakeTypeRepositoryAsync : IGenericRepositoryBaseAsync<FrontBrakeType>
    {
        public Task<(IEnumerable<FrontBrakeType> data, RecordsCount recordsCount)> GetPagedFrontBrakeTypeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
