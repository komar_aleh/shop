﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IRearStarRepositoryAsync : IGenericRepositoryBaseAsync<RearStar>
    {
        public Task<(IEnumerable<RearStar> data, RecordsCount recordsCount)> GetPagedRearStarResponseAsync(
            IntPropertyParameters requestParameters);
    }
}
