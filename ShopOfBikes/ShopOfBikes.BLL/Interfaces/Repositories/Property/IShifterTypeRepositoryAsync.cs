﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IShifterTypeRepositoryAsync : IGenericRepositoryBaseAsync<ShifterType>
    {
        public Task<(IEnumerable<ShifterType> data, RecordsCount recordsCount)> GetPagedShifterTypeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
