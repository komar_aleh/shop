﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IWheelDiameterRepositoryAsync : IGenericRepositoryBaseAsync<WheelDiameter>
    {
        public Task<(IEnumerable<WheelDiameter> data, RecordsCount recordsCount)> GetPagedWheelDiameterResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
