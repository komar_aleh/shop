﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IRimMaterialRepositoryAsync : IGenericRepositoryBaseAsync<RimMaterial>
    {
        public Task<(IEnumerable<RimMaterial> data, RecordsCount recordsCount)> GetPagedRimMaterialResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
