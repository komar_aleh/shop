﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IRearBrakeTypeRepositoryAsync : IGenericRepositoryBaseAsync<RearBrakeType>
    {
        public Task<(IEnumerable<RearBrakeType> data, RecordsCount recordsCount)> GetPagedRearBrakeTypeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
