﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IForkMaterialRepositoryAsync : IGenericRepositoryBaseAsync<ForkMaterial>
    {
        public Task<(IEnumerable<ForkMaterial> data, RecordsCount recordsCount)> GetPagedForkMaterialResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
