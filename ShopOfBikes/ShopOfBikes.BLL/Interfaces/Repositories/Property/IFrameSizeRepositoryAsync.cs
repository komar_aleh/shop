﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IFrameSizeRepositoryAsync : IGenericRepositoryBaseAsync<FrameSize>
    {
        public Task<(IEnumerable<FrameSize> data, RecordsCount recordsCount)> GetPagedFrameSizeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
