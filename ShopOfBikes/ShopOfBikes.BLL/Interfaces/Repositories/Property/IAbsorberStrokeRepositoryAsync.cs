﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IAbsorberStrokeRepositoryAsync : IGenericRepositoryBaseAsync<AbsorberStroke>
    {
        public Task<(IEnumerable<AbsorberStroke> data, RecordsCount recordsCount)> GetPagedAbsorberStrokeResponseAsync(
            IntPropertyParameters requestParameters);
    }
}
