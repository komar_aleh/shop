﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface ISaddleTypeRepositoryAsync : IGenericRepositoryBaseAsync<SaddleType>
    {
        public Task<(IEnumerable<SaddleType> data, RecordsCount recordsCount)> GetPagedSaddleTypeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
