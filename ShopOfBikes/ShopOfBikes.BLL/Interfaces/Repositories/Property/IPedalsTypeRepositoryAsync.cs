﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IPedalsTypeRepositoryAsync : IGenericRepositoryBaseAsync<PedalsType>
    {
        public Task<(IEnumerable<PedalsType> data, RecordsCount recordsCount)> GetPagedPedalsTypeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
