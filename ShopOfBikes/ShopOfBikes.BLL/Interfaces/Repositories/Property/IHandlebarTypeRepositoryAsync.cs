﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IHandlebarTypeRepositoryAsync : IGenericRepositoryBaseAsync<HandlebarType>
    {
        public Task<(IEnumerable<HandlebarType> data, RecordsCount recordsCount)> GetPagedHandlebarTypeResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
