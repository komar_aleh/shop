﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories.Property
{
    public interface IFrameColorRepositoryAsync : IGenericRepositoryBaseAsync<FrameColor>
    {
        public Task<(IEnumerable<FrameColor> data, RecordsCount recordsCount)> GetPagedFrameColorResponseAsync(
            StringPropertyParameters requestParameters);
    }
}
