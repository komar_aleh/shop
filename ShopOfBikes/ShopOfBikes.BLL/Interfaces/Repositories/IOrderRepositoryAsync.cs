﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories
{
    public interface IOrderRepositoryAsync : IGenericRepositoryBaseAsync<Order>
    {
        public Task<(IEnumerable<Order> data, RecordsCount recordsCount)> GetPagedOrderResponseAsync(
           OrdersParameters requestParameters);
    }
}
