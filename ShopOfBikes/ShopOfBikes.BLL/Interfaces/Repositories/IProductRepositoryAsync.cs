﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories
{
    public interface IProductRepositoryAsync : IGenericRepositoryBaseAsync<Product>
    {
        public Task<(IEnumerable<Product> data, RecordsCount recordsCount)> GetPagedProductResponseAsync(
            ProductsParameters requestParameters);
    }
}
