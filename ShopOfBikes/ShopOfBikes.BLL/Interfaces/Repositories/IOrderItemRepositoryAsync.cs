﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Interfaces.Repositories
{
    public interface IOrderItemRepositoryAsync : IGenericRepositoryBaseAsync<OrderItem>
    {
        public Task<(IEnumerable<OrderItem> data, RecordsCount recordsCount)> GetPagedOrderItemResponseAsync(
            OrderItemsParameters requestParameters);
    }
}
