﻿using System;
using ShopOfBikes.BLL.Parameters;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IUriHelper
    {
        public Uri GetPageUri(QueryParameters queryParameters, string route);
    }
}
