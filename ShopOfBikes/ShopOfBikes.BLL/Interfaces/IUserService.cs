﻿using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IUserService
    {
        public Task<(IEnumerable<User> data, RecordsCount recordsCount)> GetPagedUserResponseAsync(UsersParameters requestParameters);
    }
}
