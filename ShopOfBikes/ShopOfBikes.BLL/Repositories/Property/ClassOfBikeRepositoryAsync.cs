﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.Interfaces.Repositories.Property;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Repositories.Property
{
    public class ClassOfBikeRepositoryAsync : GenericRepositoryBaseAsync<ClassOfBike>, IClassOfBikeRepositoryAsync
    {
        private readonly DbSet<ClassOfBike> _database;
        public ClassOfBikeRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<ClassOfBike>();
        }

        public async Task<(IEnumerable<ClassOfBike> data, RecordsCount recordsCount)> GetPagedClassOfBikeResponseAsync(StringPropertyParameters requestParameters)
        {
            var value = requestParameters.Value;
            var pageNumber = requestParameters.PageNumber;
            var pageSize = requestParameters.PageSize;
            var orderBy = requestParameters.OrderBy;

            IQueryable<ClassOfBike> result = _database;
            var recordsTotal = result.Count();

            SearchByValue(ref result, value);
            var recordsFiltered = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToArrayAsync();

            return (resultData,recordsCount);
        }

        private static void SearchByValue(ref IQueryable<ClassOfBike> items, string value)
        {
            if (!items.Any() || string.IsNullOrEmpty(value))
                return;

            items = items.Where(o => o.Value.ToLower().Contains(value.Trim().ToLower()));
        }
    }
}
