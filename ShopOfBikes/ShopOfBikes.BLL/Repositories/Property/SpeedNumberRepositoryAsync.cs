﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.Interfaces.Repositories.Property;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Repositories.Property
{
    public class SpeedNumberRepositoryAsync : GenericRepositoryBaseAsync<SpeedNumber>, ISpeedNumberRepositoryAsync
    {
        private readonly DbSet<SpeedNumber> _database;
        public SpeedNumberRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<SpeedNumber>();
        }

        public async Task<(IEnumerable<SpeedNumber> data, RecordsCount recordsCount)> GetPagedSpeedNumberResponseAsync(StringPropertyParameters requestParameters)
        {
            var value = requestParameters.Value;
            var pageNumber = requestParameters.PageNumber;
            var pageSize = requestParameters.PageSize;
            var orderBy = requestParameters.OrderBy;

            IQueryable<SpeedNumber> result = _database;
            var recordsTotal = result.Count();

            SearchByValue(ref result, value);
            var recordsFiltered = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToArrayAsync();

            return (resultData,recordsCount);
        }

        private static void SearchByValue(ref IQueryable<SpeedNumber> items, string value)
        {
            if (!items.Any() || string.IsNullOrEmpty(value))
                return;
            items = items.Where(o => o.Value.Equals(value));
        }
    }
}
