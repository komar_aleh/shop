﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.Interfaces.Repositories;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Repositories
{
    public class ProductRepositoryAsync : GenericRepositoryBaseAsync<Product>, IProductRepositoryAsync
    {
        private readonly DbSet<Product> _database;
        public ProductRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<Product>();
        }

        public override async Task<Product> GetByIdAsync(int id)
        {
            return await _database
                .AsNoTracking()
                .AsSingleQuery()
                .Where(b => b.Id.Equals(id))
                .Include(b => b.Manufacturer)
                .Include(b => b.ClassOfBike)
                .Include(b => b.CommonDate)
                .Include(b => b.FrameMaterial)
                .Include(b => b.FrameSize)
                .Include(b => b.FrameColor)
                .Include(b => b.ForkMaterial)
                .Include(b => b.ForkModel)
                .Include(b => b.ForkType)
                .Include(b => b.WheelDiameter)
                .Include(b => b.AbsorberStroke)
                .Include(b => b.SpeedNumber)
                .Include(b => b.SystemModel)
                .Include(b => b.FrontStar)
                .Include(b => b.RearStar)
                .Include(b => b.Cassette)
                .Include(b => b.FrontSwitch)
                .Include(b => b.RearSwitch)
                .Include(b => b.Chain)
                .Include(b => b.ShifterModel)
                .Include(b => b.ShifterType)
                .Include(b => b.FrontBrakeModel)
                .Include(b => b.FrontBrakeType)
                .Include(b => b.RearBrakeModel)
                .Include(b => b.RearBrakeType)
                .Include(b => b.RimMaterial)
                .Include(b => b.TiresModel)
                .Include(b => b.HandlebarModel)
                .Include(b => b.HandlebarType)
                .Include(b => b.GripsModel)
                .Include(b => b.SaddleModel)
                .Include(b => b.SaddleType)
                .Include(b => b.PedalsModel)
                .Include(b => b.PedalsMaterial)
                .Include(b => b.PedalsType)
                .FirstOrDefaultAsync();
        }

        public async Task<(IEnumerable<Product> data, RecordsCount recordsCount)> GetPagedProductResponseAsync(ProductsParameters requestParameters)
        {
            var value = requestParameters.Name;
            var minPrice = requestParameters.MinPrice;
            var maxPrice = requestParameters.MaxPrice;
            var minCommonDate = requestParameters.MinCommonDate;
            var maxCommonDate = requestParameters.MaxCommonDate;
            var pageNumber = requestParameters.PageNumber;
            var pageSize = requestParameters.PageSize;
            var orderBy = requestParameters.OrderBy;

            IQueryable<Product> result = _database.Include(b => b.Manufacturer);
            var recordsTotal = result.Count();
            
            FilterByValue(ref result, value, minPrice, maxPrice, minCommonDate, maxCommonDate);
            var recordsFiltered = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToArrayAsync();

            return (resultData, recordsCount);
        }

        private static void FilterByValue(ref IQueryable<Product> items, string value,decimal minPrice, decimal maxPrice, int minCommonDate, int maxCommonDate)
        {
            if (!items.Any() || string.IsNullOrEmpty(value))
                return;

            items = items
                .Where(p => 
                    p.Price >= minPrice &&
                    p.Price <= maxPrice)
                .Where(p => 
                    p.CommonDate.Value >= minCommonDate && 
                    p.CommonDate.Value <= maxCommonDate)
                .Where(o => 
                    o.Name
                        .ToLower()
                        .Contains(value.Trim().ToLower()
                        )
                    );
        }
    }
}
