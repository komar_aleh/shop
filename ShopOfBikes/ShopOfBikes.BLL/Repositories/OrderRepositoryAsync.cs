﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.Interfaces.Repositories;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Repositories
{
    public class OrderRepositoryAsync : GenericRepositoryBaseAsync<Order>, IOrderRepositoryAsync
    {
        private readonly DbSet<Order> _database;
        public OrderRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<Order>();
        }

        public override async Task<Order> GetByIdAsync(int id)
        {
            return await _database.AsNoTracking()
                .AsSingleQuery()
                .Include(x => x.OrderItems)
                .ThenInclude(x => x.Product)
                .FirstOrDefaultAsync(u => u.Id.Equals(id));
        }

        public async Task<(IEnumerable<Order> data, RecordsCount recordsCount)> GetPagedOrderResponseAsync(OrdersParameters requestParameters)
        {
            var orderDate = requestParameters.OrderDate;
            var status = requestParameters.Status;
            var pageNumber = requestParameters.PageNumber;
            var pageSize = requestParameters.PageSize;
            var orderBy = requestParameters.OrderBy;

            IQueryable<Order> result = _database;
            var recordsTotal = result.Count();

           // SearchByValue(ref result, orderDate, status);
            var recordsFiltered = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToArrayAsync();

            return (resultData, recordsCount);
        }

        /*private static void SearchByValue(ref IQueryable<Order> items, DateTime orderDate, Enums.OrderStatus status)
        {
            if (!items.Any() || value == 0)
                return;
            items = items.Where(o => o.Value.Equals(value));
        }*/
    }
}
