﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.Interfaces.Repositories;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Repositories
{
    public class OrderItemRepositoryAsync : GenericRepositoryBaseAsync<OrderItem>, IOrderItemRepositoryAsync
    {
        private readonly DbSet<OrderItem> _database;
        public OrderItemRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<OrderItem>();
        }

        public override async Task<OrderItem> GetByIdAsync(int id)
        {
            return await _database.AsNoTracking()
                .AsNoTracking()
                .AsSingleQuery()
                .Include(x => x.Product)
                .FirstOrDefaultAsync(u => u.Id.Equals(id));
        }

        public async Task<(IEnumerable<OrderItem> data, RecordsCount recordsCount)> GetPagedOrderItemResponseAsync(OrderItemsParameters requestParameters)
        {
            var pageNumber = requestParameters.PageNumber;
            var pageSize = requestParameters.PageSize;
            var orderBy = requestParameters.OrderBy;

            IQueryable<OrderItem> result = _database;
            var recordsTotal = result.Count();

            var recordsFiltered = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToArrayAsync();

            return (resultData, recordsCount);
        }
    }
}
