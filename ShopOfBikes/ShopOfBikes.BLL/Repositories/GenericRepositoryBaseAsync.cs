﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.DAL.EF;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Repositories
{
    public abstract class GenericRepositoryBaseAsync<T> : IGenericRepositoryBaseAsync<T> where T : class
    {
        private readonly AppDbContext _dbContext;

        public GenericRepositoryBaseAsync(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbContext
                .Set<T>()
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllPagedAsync(int pageNumber, int pageSize)
        {
            return await _dbContext
                .Set<T>()
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<T> AddAsync(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
    }
}
