﻿namespace ShopOfBikes.BLL.Parameters
{
    public class IntPropertyParameters : QueryParameters
    {
        public int Value { get; set; }
        public override string OrderBy { get; set; } = "Value";
    }
}
