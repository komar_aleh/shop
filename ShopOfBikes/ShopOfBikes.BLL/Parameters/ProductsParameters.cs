﻿namespace ShopOfBikes.BLL.Parameters
{
    public class ProductsParameters : QueryParameters
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; } = 999999999999m;

        public int MinCommonDate { get; set; }
        public int MaxCommonDate { get; set; } = 999999999;
        public override string OrderBy { get; set; } = "Name";

        public bool ValidPriceRange => MaxPrice > MinPrice;
        public bool ValidCommonDateRange => MaxCommonDate > MinCommonDate;
        public string Name { get; set; }
    }
}
