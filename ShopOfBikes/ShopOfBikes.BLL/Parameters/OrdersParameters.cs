﻿using System;
using ShopOfBikes.DAL.Shared;

namespace ShopOfBikes.BLL.Parameters
{
    public class OrdersParameters : QueryParameters
    {
        public override string OrderBy { get; set; } = "OrderDate";
        public DateTime OrderDate { get; set; }

        public Enums.OrderStatus Status { get; set; }
    }
}
