﻿using System;
using ShopOfBikes.DAL.Shared;

namespace ShopOfBikes.BLL.Parameters
{
    public class OrderItemsParameters : QueryParameters
    {
        public override string OrderBy { get; set; } = "OrderDate";
    }
}
