﻿namespace ShopOfBikes.BLL.Parameters
{
    public class UsersParameters : QueryParameters
    {
        public string UserName { get; set; }
        public override string OrderBy { get; set; } = "UserName";
    }
}
