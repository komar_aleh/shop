﻿namespace ShopOfBikes.BLL.Parameters
{
    public class StringPropertyParameters : QueryParameters
    {
        public string Value { get; set; }
        public override string OrderBy { get; set; } = "Value";
    }
}
