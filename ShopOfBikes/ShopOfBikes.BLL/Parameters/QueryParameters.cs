﻿namespace ShopOfBikes.BLL.Parameters
{
    public class QueryParameters : PagingParameter
    {
        public virtual string OrderBy { get; set; }
    }
}
