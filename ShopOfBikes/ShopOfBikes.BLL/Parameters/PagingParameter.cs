﻿
namespace ShopOfBikes.BLL.Parameters
{
    public class PagingParameter
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        private const int maxPageSize = 200;


        public PagingParameter()
        {
            PageNumber = 1;
            PageSize = 10;
        }

        public PagingParameter(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber < 1 ? 1 : pageNumber;
            PageSize = pageSize > maxPageSize ? maxPageSize : pageSize;
        }
    }
}