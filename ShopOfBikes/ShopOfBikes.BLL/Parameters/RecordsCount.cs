﻿
namespace ShopOfBikes.BLL.Parameters
{
    public class RecordsCount
    {
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
    }
}