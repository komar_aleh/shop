﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;

        public UserService(UserManager<User> userManager)
        {
            _userManager = userManager;

        }
                
        public async Task<(IEnumerable<User> data, RecordsCount recordsCount)> GetPagedUserResponseAsync(UsersParameters requestParameters)
        {
            var name = requestParameters.UserName;
            var pageNumber = requestParameters.PageNumber;
            var pageSize = requestParameters.PageSize;
            var orderBy = requestParameters.OrderBy;

            var result = _userManager.Users;
            var recordsTotal = result.Count();
            SearchByValue(ref result, name);
            var recordsFiltered = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToArrayAsync();

            return (resultData, recordsCount);
        }

        private static void SearchByValue(ref IQueryable<User> items, string value)
        {
            if (!items.Any() || string.IsNullOrEmpty(value))
                return;

            items = items.Where(o => o.UserName.ToLower().Contains(value.Trim().ToLower()));
        }
    }
}
