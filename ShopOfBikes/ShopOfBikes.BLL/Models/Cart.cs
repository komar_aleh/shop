﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopOfBikes.DAL.Entities;

namespace ShopOfBikes.BLL.Models
{
    public class Cart
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public string UserId { get; set; }

    }
}
