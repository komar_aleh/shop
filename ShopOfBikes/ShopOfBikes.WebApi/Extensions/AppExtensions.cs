﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using ShopOfBikes.WebApi.Middlewares;

namespace ShopOfBikes.WebApi.Extensions
{
    public static class AppExtensions
    {
        private const string ImgPath = "Resources";
        public static void UseSwaggerExtension(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ShopOfBikes.WebApi");
            });
        }

        public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddleware>();
        }

        public static void UseCorsExtension(this IApplicationBuilder app)
        {
            app.UseCors();
        }

        public static void UseStaticFilesExtension(this IApplicationBuilder app)
        {
            CreateIfMissing();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
                RequestPath = new PathString("/Resources"),
                /*OnPrepareResponse = context =>
                {
                    context.Context.Response.Headers.Add("Cache-Control", "public,max-age=86400");
                }*/
            });
        }

        private static void CreateIfMissing()
        {
            var folderExists = Directory.Exists(ImgPath);
            if (!folderExists)
                Directory.CreateDirectory(ImgPath);
        }
    }
}