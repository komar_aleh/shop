﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using CodeCarvings.Piczard;
using Microsoft.Extensions.Configuration;
using static System.IO.Path;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageUploadController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ImageUploadController> _logger;

        public ImageUploadController(ILogger<ImageUploadController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        
        private const string Ext = ".jpg";

        private static readonly Dictionary<string, List<byte[]>> FileSignature = new()
        {
            { ".gif", new List<byte[]> { new byte[] { 0x47, 0x49, 0x46, 0x38 } } },
            { ".png", new List<byte[]> { new byte[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A } } },
            { ".jpeg", new List<byte[]>
                {
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE2 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE3 },
                }
            },
            { ".jpg", new List<byte[]>
                {
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE1 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE8 },
                }
            }
        };

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <returns></returns>
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult ImageUpload([FromForm] IFormFile file)
        {
            var sizeS = _configuration.GetValue<int>("ImageSizes:Small");
            var sizeM = _configuration.GetValue<int>("ImageSizes:Medium");
            var sizeL = _configuration.GetValue<int>("ImageSizes:Large");

            var sizeLimit = _configuration.GetValue<int>("SizeLimit");

            if ((file == null) || !IsValidSignature(file)) 
                throw new RequestException("Attempt to send empty form or not picture! Select Image!");

            if (file.Length > sizeLimit) 
                throw new RequestException($"File size must be less than {sizeLimit/1048576:N1} MB");

            var imageFolder = _configuration.GetValue<string>("ImageFolder");

            var folderLargeName = Combine(imageFolder, "LargeImages");
            var folderMediumName = Combine(imageFolder, "MediumImages");
            var folderSmallName = Combine(imageFolder, "SmallImages");

            var folderLargeExists = Directory.Exists(folderLargeName);
            if (!folderLargeExists)
                Directory.CreateDirectory(folderLargeName);

            var folderMediumExists = Directory.Exists(folderMediumName);
            if (!folderMediumExists)
                Directory.CreateDirectory(folderMediumName);

            var folderSmallExists = Directory.Exists(folderSmallName);
            if (!folderSmallExists)
                Directory.CreateDirectory(folderSmallName);

            var dateBegin = new DateTime(2001, 1, 1);
            var currentDate = DateTime.UtcNow;
            var elapsedTicks = currentDate.Ticks - dateBegin.Ticks;

            var fileName = elapsedTicks + Ext;

            var fullLargePath = Combine(Directory.GetCurrentDirectory(), folderLargeName, fileName);
            var fullMediumPath = Combine(Directory.GetCurrentDirectory(), folderMediumName, fileName);
            var fullSmallPath = Combine(Directory.GetCurrentDirectory(), folderSmallName, fileName);
            
            using (var stream = new FileStream(fullLargePath, FileMode.Create))
            {
                SaveAndResize(file, stream, sizeL);
            }
            using (var stream = new FileStream(fullMediumPath, FileMode.Create))
            {
                SaveAndResize(file, stream, sizeM);
            }
            using (var stream = new FileStream(fullSmallPath, FileMode.Create))
            {
                SaveAndResize(file, stream, sizeS);
            }

            _logger.LogInformation($"Image {file.FileName} was uploaded. New imageName: {fileName}");
            return StatusCode(201, new
            {
                ImgName = fileName,
                Message = "Image was uploaded in three folders: " +
                          $"'{folderLargeName}' with resolution {sizeL}*{sizeL} px, " +
                          $"'{folderMediumName}' with resolution {sizeM}*{sizeM} px, " +
                          $"'{folderSmallName}' with resolution {sizeS}*{sizeS} px"
            });
        }
        
        private static bool IsValidSignature(IFormFile file)
        {
            using var sourceImage = ImageArchiver.LoadImage(file.OpenReadStream());
            if (sourceImage.Size.Height > 5000 | sourceImage.Size.Width > 5000)
            {
                throw new RequestException($"Invalid image size {{Width={sourceImage.Size.Width}, Height={sourceImage.Size.Height}}}. The maximum image size is: {{Width=5000, Height=5000}}.");
            }

            var ext = GetExtension(file.FileName).ToLowerInvariant();

            using var reader = new BinaryReader(file.OpenReadStream());
            var signatures = FileSignature[ext];
            var headerBytes = reader.ReadBytes(signatures.Max(m => m.Length));

            return signatures.Any(signature =>
                headerBytes.Take(signature.Length).SequenceEqual(signature));
        }

        private static void SaveAndResize(IFormFile file, Stream stream, int size)
        {
            using var sourceImage = ImageArchiver.LoadImage(file.OpenReadStream());
            var filter = new FixedResizeConstraint(size, size)
            {
                CanvasColor = BackgroundColor.GetStatic(System.Drawing.Color.White),

            };
            filter.SaveProcessedImageToStream(sourceImage, stream, new JpegFormatEncoderParams(100));
        }
    }
}