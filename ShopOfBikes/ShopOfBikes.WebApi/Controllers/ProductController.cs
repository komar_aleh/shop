﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Product;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Interfaces.Repositories;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductRepositoryAsync _db;
        private readonly IUriHelper _uriService;
        private readonly ILogger<ProductController> _logger;
        public ProductController(ILogger<ProductController> logger, IMapper mapper, IProductRepositoryAsync db, IUriHelper uriService)
        {
            _logger = logger;
            _mapper = mapper;
            _db = db;
            _uriService = uriService;
        }
        
        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllProducts([FromQuery] ProductsParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }
            if (!parameters.ValidPriceRange)
            {
                throw new RequestException("Max price cannot be less than min price");
            }

            if (!parameters.ValidCommonDateRange)
            {
                throw new RequestException("Max CommonDate cannot be less than min CommonDate");
            }

            var route = Request.Path.Value;
 
            var (data, recordsCount) = await _db.GetPagedProductResponseAsync(parameters);

            var result = _mapper.Map<IEnumerable<ProductDto>>(data);
            var pagedResponse = PaginationHelper.CreatePagedResponse(result, parameters, _uriService, route, recordsCount);

            return Ok(pagedResponse);
        }
        
        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetProductById(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var product = await _db.GetByIdAsync(id);
            if (product == null)
            {
                throw new KeyNotFoundException($"Product with id={id} hasn't been found in DataBase.");
            }

            var result = _mapper.Map<ProductDetailsDto>(product);
            string classOfBike = null;
            string frameMaterial = null;
            string wheelDiameter = null;
            string forkType = null;
            string absorberStroke = null;
            string speedNumber = null;
            string weight = null;

            if (!string.IsNullOrEmpty(result.ClassOfBike?.Value))
                classOfBike = $"{result.ClassOfBike?.Value}";
            if (!string.IsNullOrEmpty(result.FrameMaterial?.Value))
                frameMaterial = $", материал рамы: {result.FrameMaterial?.Value}";
            if (!string.IsNullOrEmpty(result.WheelDiameter?.Value))
                wheelDiameter = $", колеса {result.WheelDiameter?.Value} дюймов";
            if (!string.IsNullOrEmpty(result.ForkType?.Value))
                forkType = $", вилка {result.ForkType?.Value} ";
            if (result.AbsorberStroke?.Value != 0)
                absorberStroke = $"с ходом {result.AbsorberStroke?.Value} мм";
            if (!string.IsNullOrEmpty(result.SpeedNumber?.Value))
                speedNumber = $", трансмиссия {result.SpeedNumber?.Value} скор.";
            var breakType = $", тормоза: {result.FrontBrakeType.Value} + {result.RearBrakeType.Value}";
            if (result.Weight != 0)
                weight = $", {result.Weight:вес: 0.##} кг";

            result.Description = classOfBike + frameMaterial + wheelDiameter + forkType + absorberStroke +
                                 speedNumber + breakType + weight;

            return Ok(new Response<ProductDetailsDto>(result));
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> CreateProduct([FromBody] ProductForCreationDto item)
        {
            if (item == null)
            {
                throw new RequestException("Product object is null");
            }
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid Product object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }
            var itemEntity = _mapper.Map<Product>(item);

            await _db.AddAsync(itemEntity);

            var createdItem = _mapper.Map<ProductDto>(itemEntity);

            _logger.LogInformation($"New product with id={createdItem.Id} was created.");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()), new Response<ProductDto>(createdItem));
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var item = await _db.GetByIdAsync(id);
            if (item == null)
            {
                throw new KeyNotFoundException($"Product with id={id} hasn't been found in DataBase.");
            }

            await _db.DeleteAsync(item);
            _logger.LogInformation($"Product with id={id} deleted from DataBase");

            return Ok(new Response<ProductDto>()
            {
                Succeeded = true,
                Message = $"Product with id={id} was deleted from DataBase"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> UpdateProduct(int id, [FromBody] ProductForUpdateDto item)
        {
            if (item == null)
            {
                throw new RequestException("Product object is null");
            }
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid Product object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }
            var productEntity = await _db.GetByIdAsync(id);
            if (productEntity == null)
            {
                throw new KeyNotFoundException($"Product with id={id} hasn't been found in DataBase.");
            }

            _mapper.Map(item, productEntity);

            await _db.UpdateAsync(productEntity);
            _logger.LogInformation($"Product id={id} updated in DataBase.");

            var updatedProduct = _mapper.Map<ProductDto>(productEntity);
            return Ok(new Response<ProductDto>(updatedProduct));
        }
    }
}
