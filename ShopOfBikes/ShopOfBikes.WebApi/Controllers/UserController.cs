﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ShopOfBikes.BLL.DTO.User;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Shared;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUserService _users;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly IUriHelper _uriService;
        private readonly ILogger<UserController> _logger;
        public UserController(ILogger<UserController> logger, IMapper mapper,  IUriHelper uriService,
            UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, IUserService users)
        {
            _logger = logger;
            _mapper = mapper;
            _uriService = uriService;
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _users = users;
        }

        /// <summary>
        /// POST api/controller/register
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterDto model)
        {
            if (model == null)
            {
                throw new RequestException("User object is null");
            }
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid user object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var userExists = await _userManager.FindByNameAsync(model.UserName);
            if (userExists != null)
            {
                throw new RequestException("User already exists!");
            }

            var user = new User
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.UserName
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new RequestException("User creation failed! Please check user details and try again.");
            }

            if (!await _roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
            }

            if (!await _roleManager.RoleExistsAsync(UserRoles.User))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.User));
            }

            if (await _roleManager.RoleExistsAsync(UserRoles.User))
            {
                await _userManager.AddToRoleAsync(user, UserRoles.User);
            }

            var createdUser = _mapper.Map<UserRegisterDto>(model);
            var respOk = new Response<UserRegisterDto>(createdUser)
            {
                Succeeded = true,
                Message = "User created successfully!"
            };
            _logger.LogInformation("New user was created");
            var baseUrl = Request.Path.Value?.ToLower().Remove(9);
            return Created(string.Concat(baseUrl, "/", user.Id), respOk);
        }
        /// <summary>
        /// POST api/controller/reg-admin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles = UserRoles.Admin)]
        [Route("reg-admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] UserRegisterDto model)
        {
            if (model == null)
            {
                throw new RequestException("User object is null");
            }
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid user object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var userExists = await _userManager.FindByNameAsync(model.UserName);

            if (userExists != null)
            {
                throw new RequestException("User already exists!");
            }

            var user = new User
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.UserName
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new RequestException("User creation failed! Please check user details and try again.");
            }

            if (!await _roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
            }

            if (!await _roleManager.RoleExistsAsync(UserRoles.User))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.User));
            }

            if (await _roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await _userManager.AddToRoleAsync(user, UserRoles.Admin);
            }
            
            var createdUser = _mapper.Map<UserRegisterDto>(model);
            
            var respOk = new Response<UserRegisterDto>(createdUser)
            {
                Succeeded = true,
                Message = "User with role 'Admin' created successfully!"
            };
            _logger.LogInformation("New user with role 'Admin' was created");
            var baseUrl = Request.Path.Value?.ToLower().Remove(9);
            return Created(string.Concat(baseUrl, "/", user.Id), respOk);
        }

        /// <summary>
        /// POST api/controller/login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] UserForLoginDto model)
        {
            if (model == null)
            {
                throw new RequestException("User object is null");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid user object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var user = await _userManager.FindByNameAsync(model.UserName);

            if (user == null || !await _userManager.CheckPasswordAsync(user, model.Password)) return Unauthorized();

            var userRoles = await _userManager.GetRolesAsync(user);
            var expiresDays = DateTime.Now.AddDays(Convert.ToInt32(_configuration["JWT:ExpireDays"]));
            var key = Encoding.UTF8.GetBytes(_configuration["JWT:SecretKey"]);
            var signingKey = new SymmetricSecurityKey(key);
            var credentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var authorizationClaims = new List<Claim>
            {
                new(ClaimTypes.Name, user.UserName),
                new(JwtRegisteredClaimNames.Email, user.Email),
                new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new(JwtRegisteredClaimNames.Exp, expiresDays.ToString(CultureInfo.InvariantCulture))
            };
            authorizationClaims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole)));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ServerUrl"],
                audience: _configuration["JWT:ServerUrl"],
                expires: expiresDays,
                claims: authorizationClaims,
                signingCredentials: credentials);
            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                userName = user.UserName,
                expiration = token.ValidTo,
                Status = "Success",
                Message = "Authorization was successful!"
            });
        }
        
        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllUsers([FromQuery] UsersParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var route = Request.Path.Value;

            var (data, recordsCount) = await _users.GetPagedUserResponseAsync(parameters);

            var result = _mapper.Map<IEnumerable<UserDto>>(data);
            var pagedResponse = PaginationHelper.CreatePagedResponse(result, parameters, _uriService, route, recordsCount);

            return Ok(pagedResponse);
        }
        
        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetUserById(string id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var user = await _userManager.Users
                .AsNoTracking()
                .AsSingleQuery()
                .Where(u => u.Id.Equals(id))
                .Include(u => u.Orders)
                .ThenInclude(u => u.OrderItems)
                .ThenInclude(u => u.Product)
                .FirstOrDefaultAsync();
            if (user == null)
            {
                throw new KeyNotFoundException($"User with id={id} hasn't been found in DataBase.");
            }

            var result = _mapper.Map<UserWithOrdersDto>(user);
            return Ok(new Response<UserWithOrdersDto>(result)
            {
                Message = $"Returned user with username: {result.UserName}"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> UpdateUser(string id, [FromBody] UserForUpdateDto user)
        {
            if (user == null)
            {
                throw new RequestException("User object is null");
            }
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }
            var userEntity = await _userManager.FindByIdAsync(id);
            if (userEntity == null)
            {
                throw new KeyNotFoundException($"User with id={id} hasn't been found in DataBase.");
            }

            _mapper.Map(user, userEntity);
            var result = await _userManager.UpdateAsync(userEntity);
            if (!result.Succeeded)
            {
                throw new RequestException("User creation failed! Please check user details and try again.");
            }
            var updatedUser = _mapper.Map<UserDto>(userEntity);

            _logger.LogInformation($"User with id={id} updated in DataBase");
            return Ok(new Response<UserDto>(updatedUser));
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new KeyNotFoundException($"User with id={id} hasn't been found in DataBase.");
            }

            await _userManager.DeleteAsync(user);
            _logger.LogInformation($"User with id={id} deleted from DataBase");

            return Ok(new Response<UserDto>($"User with id={id} was deleted from DataBase")
            {
                Succeeded = true,
            });
        }
    }
}
