﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Interfaces.Repositories;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItemController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IOrderItemRepositoryAsync _db;
        private readonly IUriHelper _uriService;
        private readonly ILogger<OrderItemController> _logger;
        public OrderItemController(ILogger<OrderItemController> logger, IMapper mapper, IUriHelper uriService,IOrderItemRepositoryAsync db)
        {
            _logger = logger;
            _mapper = mapper;
            _db = db;
            _uriService = uriService;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllOrderItems([FromQuery] OrderItemsParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var route = Request.Path.Value;

            var (data, recordsCount) = await _db.GetPagedOrderItemResponseAsync(parameters);


            var result = _mapper.Map<IEnumerable<OrderItemDto>>(data);
            var pagedResponse = PaginationHelper.CreatePagedResponse(result, parameters, _uriService, route, recordsCount);

            return Ok(pagedResponse);
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "OrderItemById")]
        [Produces("application/json")]
        public async Task<IActionResult> GetOrderItemById(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }
            var orderItem = await _db.GetByIdAsync(id);
            if (orderItem == null)
            {
                throw new KeyNotFoundException($"Order with id={id} hasn't been found in DataBase.");
            }

            var result = _mapper.Map<OrderItemDto>(orderItem);
            return Ok(new Response<OrderItemDto>(result));
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> CreateOrderItem([FromBody] OrderItemForCreationDto item)
        {
            if (item == null)
            {
                throw new RequestException("OrderItem object is null");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid OrderItem object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var itemEntity = _mapper.Map<OrderItem>(item);
            await _db.AddAsync(itemEntity);
            var createdItem = _mapper.Map<OrderItemDto>(itemEntity);

            _logger.LogInformation($"New OrderItem was created. Id = {createdItem.Id}");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()), new Response<OrderItemDto>(createdItem)
                { Message = $"Item with value '{createdItem.Id}' was created" });
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderItem(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var item = await _db.GetByIdAsync(id);
            if (item == null)
            {
                throw new KeyNotFoundException($"OrderItem with id={id} hasn't been found in DataBase.");
            }

            await _db.DeleteAsync(item);
            _logger.LogInformation($"OrderItem with id={id} deleted from DataBase");

            return Ok(new Response<OrderItemDto>
            {
                Succeeded = true,
                Message = $"OrderItem with id={id} was deleted from DataBase"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> UpdateOrderItem(int id, [FromBody] OrderItemForUpdateDto item)
        {
            if (item == null)
            {
                throw new RequestException("OrderItem object is null");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid OrderItem object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var itemEntity = await _db.GetByIdAsync(id);
            if (itemEntity == null)
            {
                throw new KeyNotFoundException($"OrderItem with id={id} hasn't been found in DataBase.");
            }

            _mapper.Map(item, itemEntity);
            await _db.UpdateAsync(itemEntity);
            var updatedItem = _mapper.Map<OrderItemDto>(itemEntity);

            _logger.LogInformation($"OrderItem with id={id} updated in DataBase.");
            return Ok(new Response<OrderItemDto>(updatedItem));
        }
    }
}
