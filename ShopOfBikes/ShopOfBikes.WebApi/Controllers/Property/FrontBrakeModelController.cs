﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Interfaces.Repositories.Property;


namespace ShopOfBikes.WebApi.Controllers.Property
{
    [Route("api/[controller]")]
    [ApiController]
    public class FrontBrakeModelController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFrontBrakeModelRepositoryAsync _db;
        private readonly ILogger<FrontBrakeModelController> _logger;
        private readonly IUriHelper _uriService;

        public FrontBrakeModelController(
            IFrontBrakeModelRepositoryAsync db,
            ILogger<FrontBrakeModelController> logger,
            IMapper mapper, IUriHelper uriService
            )
        {
            _db = db;
            _logger = logger;
            _mapper = mapper;
            _uriService = uriService;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllFrontBrakeModels([FromQuery] StringPropertyParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var route = Request.Path.Value;

            var (data, recordsCount) = await _db.GetPagedFrontBrakeModelResponseAsync(parameters);

            var result = _mapper.Map<IEnumerable<PropertyStringValueDto>>(data);
            var pagedResponse = PaginationHelper.CreatePagedResponse(result, parameters, _uriService, route, recordsCount);

            return Ok(pagedResponse);
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetFrontBrakeModelById(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }
            var item = await _db.GetByIdAsync(id);
            if (item == null)
            {
                throw new KeyNotFoundException($"FrontBrakeModel with id={id} hasn't been found in DataBase.");
            }

            var result = _mapper.Map<PropertyStringValueDto>(item);
            return Ok(new Response<PropertyStringValueDto>(result));
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> CreateFrontBrakeModel([FromBody] PropertyStringValueForCreationUpdateDto item)
        {
            if (item == null)
            {
                throw new RequestException("FrontBrakeModel object is null");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid FrontBrakeModel object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var itemEntity = _mapper.Map<FrontBrakeModel>(item);
            await _db.AddAsync(itemEntity);
            var createdItem = _mapper.Map<PropertyStringValueDto>(itemEntity);

            _logger.LogInformation($"New FrontBrakeModel was created. Id = {createdItem.Id}");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()), new Response<PropertyStringValueDto>(createdItem)
                { Message = $"Item with value '{createdItem.Value}' was created" });
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFrontBrakeModel(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var item = await _db.GetByIdAsync(id);
            if (item == null)
            {
                throw new KeyNotFoundException($"FrontBrakeModel with id={id} hasn't been found in DataBase.");
            }

            await _db.DeleteAsync(item);
            _logger.LogInformation($"FrontBrakeModel with id={id} deleted from DataBase");

            return Ok(new Response<PropertyStringValueDto>
            {
                Succeeded = true,
                Message = $"FrontBrakeModel with id={id} was deleted from DataBase"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> UpdateFrontBrakeModel(int id, [FromBody] PropertyStringValueForCreationUpdateDto item)
        {
            if (item == null)
            {
                throw new RequestException("FrontBrakeModel object is null");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid FrontBrakeModel object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var itemEntity = await _db.GetByIdAsync(id);
            if (itemEntity == null)
            {
                throw new KeyNotFoundException($"FrontBrakeModel with id={id} hasn't been found in DataBase.");
            }

            _mapper.Map(item, itemEntity);
            await _db.UpdateAsync(itemEntity);
            var updatedItem = _mapper.Map<PropertyStringValueDto>(itemEntity);

            _logger.LogInformation($"FrontBrakeModel with id={id} updated in DataBase. Updated value - {item.Value}");
            return Ok(new Response<PropertyStringValueDto>(updatedItem));
        }
    }
}
