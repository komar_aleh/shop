﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Order;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Parameters;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Interfaces.Repositories;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IOrderRepositoryAsync _db;
        private readonly IUriHelper _uriService;
        private readonly ILogger<OrderController> _logger;

        public OrderController(ILogger<OrderController> logger, IMapper mapper, IOrderRepositoryAsync db, IUriHelper uriService)
        {
            _logger = logger;
            _mapper = mapper;
            _db = db;
            _uriService = uriService;
        }
        
        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllOrders([FromQuery] OrdersParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var route = Request.Path.Value;

            var(data, recordsCount) = await _db.GetPagedOrderResponseAsync(parameters);
            
            var result = _mapper.Map<IEnumerable<OrderDto>>(data);
            var pagedResponse = PaginationHelper.CreatePagedResponse(result, parameters, _uriService, route, recordsCount);

            return Ok(pagedResponse);
        }
        
        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetOrderById(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }
            var item = await _db.GetByIdAsync(id);
            if (item == null)
            {
                throw new KeyNotFoundException($"Order with id={id} hasn't been found in DataBase.");
            }

            var result = _mapper.Map<OrderDto>(item);
            return Ok(new Response<OrderDto>(result));
        }
        
        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> CreateOrder([FromBody] OrderForCreationDto item)
        {
            if (item == null)
            {
                throw new RequestException("Order object is null");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid Order object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var itemEntity = _mapper.Map<Order>(item);
            await _db.AddAsync(itemEntity);
            var createdItem = _mapper.Map<OrderAfterCreationDto>(itemEntity);

            _logger.LogInformation($"New Order was created. Id = {createdItem.Id}");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()), new Response<OrderAfterCreationDto>(createdItem)
                { Message = $"Item with value '{createdItem.Id}' was created" });
        }
        
        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            if (!ModelState.IsValid)
            {
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var item = await _db.GetByIdAsync(id);
            if (item == null)
            {
                throw new KeyNotFoundException($"Order with id={id} hasn't been found in DataBase.");
            }

            await _db.DeleteAsync(item);
            _logger.LogInformation($"Order with id={id} deleted from DataBase");

            return Ok(new Response<OrderDto>
            {
                Succeeded = true,
                Message = $"Order with id={id} was deleted from DataBase"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> UpdateOrder(int id, [FromBody] OrderForUpdateDto item)
        {
            if (item == null)
            {
                throw new RequestException("Order object is null");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid Order object sent from client.");
                var failures = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
                throw new ValidationException(failures);
            }

            var itemEntity = await _db.GetByIdAsync(id);
            if (itemEntity == null)
            {
                throw new KeyNotFoundException($"Order with id={id} hasn't been found in DataBase.");
            }

            _mapper.Map(item, itemEntity);
            await _db.UpdateAsync(itemEntity);
            var updatedItem = _mapper.Map<OrderDto>(itemEntity);

            _logger.LogInformation($"Order with id={id} updated in DataBase. Updated status - {item.Status}");
            return Ok(new Response<OrderDto>(updatedItem));
        }
    }
}
