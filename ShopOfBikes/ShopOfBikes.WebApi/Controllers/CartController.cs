﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ShopOfBikes.BLL.Interfaces.Repositories;
using ShopOfBikes.BLL.Models;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IProductRepositoryAsync _productService;
        private readonly IOrderItemRepositoryAsync _orderService;
        private readonly ILogger<CartController> _logger;

        public CartController(ILogger<CartController> logger, IProductRepositoryAsync productService, IOrderItemRepositoryAsync orderService)
        {
            _productService = productService;
            _orderService = orderService;
            _logger = logger;
        }


        

    }
}
