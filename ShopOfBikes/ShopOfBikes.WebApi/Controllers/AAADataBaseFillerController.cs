﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShopOfBikes.BLL.Interfaces.Repositories;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AAADataBaseFillerController : ControllerBase
    {
        private readonly IProductRepositoryAsync _productRepository;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<AAADataBaseFillerController> _logger;

        public AAADataBaseFillerController(ILogger<AAADataBaseFillerController> logger, IProductRepositoryAsync productRepository, 
            UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _productRepository = productRepository;
            _logger = logger;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        /// <summary>
        ///     Fill data in DataBase
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> FillData()
        {
            var product1 = new Product()
            {
                Name = "Marlin 5 29 M 2021",
                ManufacturerId = 90,
                ClassOfBikeId = 2,
                CommonDateId = 5,
                Weight = 14.44f,
                HardTail = false,
                Female = false,
                KidTeen = false,
                DoubleRim = true,
                ImgName = "6577191154194605.jpg",
                TiresTubeless = false,
                FrameMaterialId = 1,
                FrameSizeId = 71,
                FrameColorId = 30,
                ForkMaterialId = 1,
                ForkModelId = 181,
                ForkTypeId = 2,
                ForkLock = true,
                WheelDiameterId = 6,
                AbsorberStrokeId = 7,
                SpeedNumberId = 8,
                SystemModelId = 110,
                FrontStarId = 3,
                RearStarId = 4,
                Cassette = new Cassette() { Value = "Shimano  (HG200)" },
                FrontSwitch = new FrontSwitch() { Value = "Shimano Tourney  (TY300)" },
                RearSwitch = new RearSwitch() { Value = "Shimano Tourney  (TY300)" },
                Chain = new Chain() { Value = "KMC  (Z7)" },
                ShifterModelId = 19,
                ShifterTypeId = 1,
                FrontBrakeModelId = 105,
                FrontBrakeTypeId = 2,
                RearBrakeModelId = 115,
                RearBrakeTypeId = 3,
                RimMaterialId = 1,
                TiresModelId = 15,
                HandlebarModel = new HandlebarModel() { Value = "Bontrager" },
                HandlebarTypeId = 4,
                GripsModel = new GripsModel() { Value = "Bontrager  (XR Endurance Comp)" },
                SaddleTypeId = 1,
                SaddleModel = new SaddleModel() { Value = "Bontrager  (Arvada)" },
                PedalsTypeId = 1,
                PedalsModel = new PedalsModel() { Value = "VP  (VP-536)" },
                PedalsMaterialId = 1,
                Quantity = 5,
                Price = 1660
            };

            await _productRepository.AddAsync(product1);

            var user1 = new User()
            {
                FirstName = "Ivan",
                LastName = "Ivanov",
                UserName = "IvanIvanov",
                Email = "iviv@test.ru",
                Orders = new List<Order>()
                    {
                        new()
                        {
                            OrderItems = new List<OrderItem>()
                            {
                                new()
                                {
                                    ProductId = product1.Id,
                                    Quantity = 1
                                }
                            }
                        }
                    }
            };

            var user2 = new User()
            {
                FirstName = "Petr",
                LastName = "Petrov",
                UserName = "PetrPetrov",
                Email = "pepetr@test.ru",
                Orders = new List<Order>()
                    {
                        new()
                        {
                            OrderItems = new List<OrderItem>()
                            {
                                new()
                                {
                                    ProductId = product1.Id,
                                    Quantity = 1
                                }
                            }
                        }
                    }
            };
            var user3 = new User()
            {
                FirstName = "Alexiy",
                LastName = "Kupalov",
                UserName = "MaximSidorov",
                Email = "msid@test.ru"
            };
            var user4 = new User()
            {
                FirstName = "Alexandr",
                LastName = "Kupalinskiy",
                Email = "sobaka2@gov.by",
                UserName = "alexKup",
                Orders = new List<Order>()
                    {
                        new()
                        {
                            OrderItems = new List<OrderItem>()
                            {
                                new()
                                {
                                    ProductId = product1.Id,
                                    Quantity = 1
                                }
                            },

                        }
                    }
            };

            await _userManager.CreateAsync(user1, "st%Yring1");
            await _userManager.CreateAsync(user2, "st%Yring1");
            await _userManager.CreateAsync(user3, "st%Yring1");
            await _userManager.CreateAsync(user4, "st%Yring1");
            if (!await _roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
            }

            if (!await _roleManager.RoleExistsAsync(UserRoles.User))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.User));
            }
            
            await _userManager.AddToRoleAsync(user1, UserRoles.User);
            await _userManager.AddToRoleAsync(user2, UserRoles.User);
            await _userManager.AddToRoleAsync(user3, UserRoles.User);
            await _userManager.AddToRoleAsync(user4, UserRoles.User);
            
            _logger.LogInformation("All data insert to DataBase.");

            return NoContent();
        }
    }
}
