﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddleware> _logger;

        public ErrorHandlerMiddleware(RequestDelegate next, ILogger<ErrorHandlerMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                var responseModel = new Response<string> { Succeeded = false, Message = error.Message };

                switch (error)
                {
                    case RequestException e:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        _logger.LogError($"{error.Message} {e.InnerException?.Message}");
                        break;

                    case ValidationException e:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        responseModel.Errors = e.Errors;
                        _logger.LogError($"{error.Message}");
                        break;

                    case KeyNotFoundException:
                        response.StatusCode = (int)HttpStatusCode.NotFound;
                        _logger.LogError($"{error.Message}");
                        break;
                    case DbUpdateException e:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        responseModel.Message = $"{e.InnerException?.Message}";
                        _logger.LogError(responseModel.Message);
                        break;

                    default:
                        // unhandled error
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        _logger.LogError($"Something went wrong. {error.Message}");
                        break;
                }
                var result = JsonSerializer.Serialize(responseModel);

                await response.WriteAsync(result);
            }
        }
    }
}