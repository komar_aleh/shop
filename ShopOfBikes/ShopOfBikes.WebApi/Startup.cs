using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using ShopOfBikes.BLL.Extensions;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Extensions;
using ShopOfBikes.DAL.Shared;
using ShopOfBikes.WebApi.Extensions;
using System.Globalization;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace ShopOfBikes.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
            services.AddBusinessLogicLayer();
            services.AddDataLogicLayer(Configuration);
            services.AddSwaggerExtension();
            //services.AddMemoryCache();
            services.AddControllersExtension();
            services.AddCorsExtension();
            services.AddJwtAuthentication(Configuration);
            
            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppDbContext dbContext, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            dbContext.Database.EnsureCreated();
            // заполнение первоначальными данными
            SeedData(userManager, roleManager);
            
            app.UseStaticFilesExtension();
            
            app.UseRouting();
            app.UseCorsExtension();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseExceptionHandler("/error");
            app.UseErrorHandlingMiddleware();
            app.UseSwaggerExtension();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            var lastConnection = Configuration.GetValue<string>("LastConnection");
            var sqlConnectionString = Configuration.GetConnectionString("DefaultConnection");
            if (lastConnection == sqlConnectionString) return;
            var file = new FileInfo("Resources\\insertData.sql");
            var script = file.OpenText().ReadToEnd();
            using var connection = new SqlConnection(sqlConnectionString);
            connection.Open();
            var command = new SqlCommand(script, connection);
            command.ExecuteNonQuery();

            var email = Configuration["AdminData:Email"];
            var password = Configuration["AdminData:Password"];
            var user = new User
            {
                FirstName = "Administrator",
                LastName = "Admin",
                UserName = email,
                Email = email
            };
            if (!roleManager.RoleExistsAsync(UserRoles.Admin).GetAwaiter().GetResult())
            {
                var roleResult = roleManager.CreateAsync(new IdentityRole(UserRoles.Admin)).Result;
            }
            var userExistsResult = userManager.FindByEmailAsync(email).GetAwaiter().GetResult();
            if (userExistsResult != null) return;
            var result = userManager.CreateAsync(user, password).GetAwaiter().GetResult();
            if (result.Succeeded)
            {
                userManager.AddToRoleAsync(user, UserRoles.Admin).Wait();
            }

            var appSettingsJsonFilePath = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            var config = File.ReadAllText(appSettingsJsonFilePath);
            dynamic jsonObj = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(config);

            jsonObj["LastConnection"] = sqlConnectionString;

            var output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
            File.WriteAllText(appSettingsJsonFilePath, output);
        }
    }
}
